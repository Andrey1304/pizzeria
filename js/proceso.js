async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}
mostrar();
function mostrar() {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/pizzeria.json";
    leerJSON(url).then(datos => {
        let titulo = "";
        var title = document.getElementById("titulo");
        titulo = datos.nombrePizzeria;
        title.innerHTML = '<h1>' + titulo + '</h1>';
    })
}

function ingresarPizzeria() {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/pizzeria.json";
    leerJSON(url).then(datos => {

        var adicional = datos.adicional;
        var pizzas = datos.pizzas;
        crearPizzas(datos.pizzas);


    })
}
function cargarOpciones() {
    let url = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/pizzeria.json";
    leerJSON(url).then(datos => {

        MostrarAdicional(datos.pizzas, datos.adicional);
        leerImagenes(datos.pizzas);

    })
}



function crearPizzas(pizzas) {
    let np = document.getElementById("npizzas").value;
    let tabla = document.getElementById("table_div2");
    tabla.innerHTML = '';
    for (let c = 0; c < np; c++) {
        tabla.innerHTML += '<div class="card" style="width: 70rem;"><div class="card-body"><div class="row justify-content-start align-items-center text-center"><div class="col-4"><p class="card-text">Tamaño de pizza:</p></div><div class="col-2 col-sm-2 col-xl-2 col-md-2 col-lg-2"><select class="form-control" value="2" type="number" id="tamaniosescogidos' + c + '" name="datos" placeholder="0 " required/>' + '<option>' + guardarTamano(pizzas)[0] + '</option><option>' + guardarTamano(pizzas)[1] + '</option><option>' + guardarTamano(pizzas)[2] + '</option></select></div></div>';


    }

    tabla.innerHTML += '</br><div class="row justify-content-end align-items-center text-center"><div class="col-12"><button type="button" onclick="cargarOpciones()" class="btn btn-success">Cargar Opciones</button></div></div>';

}

function guardarPreciosDeTamanoSeleccionados() {
    let tamanios = [];
    let np = document.getElementById("npizzas").value;
    for (let c = 0; c < np; c++) {

        let tamanio = document.getElementById(("tamaniosescogidos" + c)).value;
        tamanios.push(tamanio);
    }
    return tamanios;
}



function MostrarAdicional(pizzas, adicional) {

    guardarPreciosDeTamanoSeleccionados();
    let np = document.getElementById("npizzas").value;
    let tabla = document.getElementById("table_div2");
    tabla.innerHTML = '';

    for (let c = 0; c < np; c++) {
        tabla.innerHTML += '</br> <div class="card border-dark" style="width: 70rem; border-width:2px;"> <div class="card-body"> <div class="row justify-content-start align-items-center text-center">'
            + '<div class="col-6"> <p class="card-text">Escoja sabores para pizza 2 (puede escoger uno o dos):</p> </div> <div class="col-3 col-sm-3 col-xl-2 col-md-2 col-lg-2"> <select id="sabor1" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example"> <option selected>' + leerSabor(pizzas)[0] + '</option> <option value="1">' + leerSabor(pizzas)[1] + '</option> <option value="2">' + leerSabor(pizzas)[2] + '</option> <option value="2">' + leerSabor(pizzas)[3] + '</option></select> </div> <div class="col-3 col-sm-3 col-xl-2 col-md-2 col-lg-2"> <select id="sabor2" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">'
            + '<option selected>Ninguno</option> <option value="1">' + leerSabor(pizzas)[0] + '</option> <option value="2">' + leerSabor(pizzas)[1] + '</option> <option value="3">' + leerSabor(pizzas)[2] + '</option> <option value="3">' + leerSabor(pizzas)[3] + '</option></select> </div> </div> <br><br><br><br><br> <div class="row justify-content-start align-items-center text-center"> <div class="col-8"> <p class="card-text row justify-content-start">Ingredientes adicionales(Pizza 1: .... ):</p> <label> <input type="checkbox" id="cbox1" value="primer_checkbox">' + leerAdicional(adicional)[0] + '</label> <input type="checkbox" id="cbox2" value="segundo_checkbox"> <label for="cbox2">' + leerAdicional(adicional)[1] + '</label> <input type="checkbox" id="cbox3" value="tercer_checkbox"> <label for="cbox2">' + leerAdicional(adicional)[2] + '</label> <input type="checkbox" id="cbox4" value="cuarto_checkbox">'
            + '<label for="cbox2">' + leerAdicional(adicional)[3] + '</label> <br> <br> <br> <p class="card-text row justify-content-start">Ingredientes adicionales(Pizza 2: .... ):</p> <label> <input type="checkbox" id="cbox1" value="primeer_checkbox">' + leerAdicional(adicional)[0] + '</label> <input type="checkbox" id="cbox2" value="segundo_checkbox"> <label for="cbox2">' + leerAdicional(adicional)[1] + '</label> <input type="checkbox" id="cbox3" value="tercer_checkbox"> <label for="cbox2">' + leerAdicional(adicional)[2] + '</label> <input type="checkbox" id="cbox4" value="cuarto_checkbox"> <label for="cbox2">' + leerAdicional(adicional)[3] + '</label> </div> <div id="imagen1" class="col-2">  </div> <div id="imagen2" class="col-2">  </div>'
      
        + '</div> </div> </div> </div> <br> </br>';
        
    }
    eligaImagen1(pizzas);
    eligaImagen2(pizzas);
    tabla.innerHTML += '<div class="row justify-content-end align-items-center text-center"> <div class="col-12"> <button typ="button" onclick="calcularFactura()" class="btn btn-success"> Calcular factura </button> </div> </div></br>';

}
function eligaImagen1(pizzas) {
    let s1 = document.getElementById("sabor1").value;
    let imag1 = document.getElementById("imagen1");
    imag1.innerHTML = '';
    for (let j = 0; j < 5; j++) {
        if (s1 == leerSabor(pizzas)[j]) {
            imag1.innerHTML =leerImagenes(pizzas)[j];
        }
    }

}
function eligaImagen2(pizzas) {
    let s2 = document.getElementById("sabor2").value;
    let imag2 = document.getElementById("imagen2");
    imag2.innerHTML = '';
    for (let j = 0; j < 6; j++) {
        if (s2 == leerSabor(pizzas)[j]) {
            imag2.innerHTML =leerImagenes(pizzas)[j];
        }
    }

}



function guardarTamano(pizzas) {
    let precios;
    for (let i = 0; i < pizzas.length; i++) {
        precios = pizzas[i].precio;
    }
    let tamanios = [];
    for (let i = 0; i < precios.length; i++) {
        tamanios[i] = precios[i].tamano;
    }
    return tamanios;
}



function leerAdicional(adicional) {
    let adicionales = [];
    for (let i = 0; i < adicional.length; i++) {
        adicionales[i] = adicional[i].nombre_ingrediente;
    }
    return adicionales;
}

function leerSabor(pizzas) {
    let sabores = [];
    for (let i = 0; i < pizzas.length; i++) {
        sabores[i] = pizzas[i].sabor;
    }
    return sabores;
}


function leerImagenes(pizzas) {
    let imagen = [];
    for (let i = 0; i < pizzas.length; i++) {
        for (let j = 0; j < pizzas.length; j++) {
            imagen[i] = '<div class="col-2"> <img src="' + pizzas[i].url_Imagen + '" alt="..." class="img-thumbnail"> </div> ';
        }
    }

    return imagen;
}



function cargarDeNuevo() {
    document.getElementById("npizzas").value = "";
    crearPizzas();
}

/*
function leerPizzas(pizzas) {
    var msg = "";
    for (let i = 0; i < pizzas.length; i++) {

        msg += "<hr>" + pizzas[i].sabor + ",<a href='" + pizzas[i].url_Imagen + "'>Ver imagen</a>";
        let precios = pizzas[i].precio;
        msg += leerPrecios(precios);
        guardarTamano(precios);
    }
    return msg;
}

function leerPrecios(precios) {
    var msg = "";
    for (let i = 0; i < precios.length; i++) {
        msg += "<br> Tamaño:" + precios[i].tamano + ", Precio:" + precios[i].precio;
    }
    return msg;
}

function leerAdicional(adicional) {
    var msg = "<hr><br>Ingredientes adicionales</br>";
    for (let i = 0; i < adicional.length; i++) {
        msg += "<br>" + adicional[i].nombre_ingrediente + "," + adicional[i].valor;
    }
    return msg;
}



*/











